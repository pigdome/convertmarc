use strict;
use warnings;
use Switch;
use Path::Class;
use autodie;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;




convertErrMARCToMARC( getResource() );


sub convertErrMARCToMARC
{
    my ($file_handle_output_log,$file_handle_output_marc,$batch) = @_;

    while ( my $record = $batch->next() )
    {
    	my $bibnumber =  $record->field('001')->as_string();
    	print "Biblionumber " . $bibnumber . " OK\n";
    	my $record_out = MARC::Record->new();
    	my $leader = $record->leader();
    	$record_out->leader($leader);
  
        my @log = ();
    	for (my $i = 0; $i < 10; $i++) 
	    {
	    	my $tag = $i . "..";
	    	foreach my $field ($record->field($tag)) 
		    {
		       $record_out->append_fields(newFieldLower10($record,$field,$bibnumber,$file_handle_output_log)) if( $field->tag() < 10 ); 	
		       $record_out->append_fields(newFieldUpper10($record,$field,$bibnumber,$file_handle_output_log) )if( $field->tag() >= 10 );
		    }	
	    }
	    $file_handle_output_marc->print($record_out->as_usmarc() . "\n");
	    
	    skipRecord($batch,$bibnumber); 
    }
        
}




sub getResource
{
    if( scalar @ARGV != 3 )
    {
    	print "\n\n Usage : perl convert.pl [input.mrc] [output.mrc] [log.txt] \n";
    	exit;
    }

	# -------- LOG FILE -------------#
    my $path_log = substr($ARGV[2],0,rindex($ARGV[2],"/") ) if( index($ARGV[2],"/") >= 0 );
    my $filename_log = substr($ARGV[2],rindex($ARGV[2],"/"),length($ARGV[2]) ) if( defined $path_log );
	my $dir_output_log = dir($path_log);
	my $file_output_log = $dir_output_log->file($filename_log);
	my $file_handle_output_log = $file_output_log->openw();
    
    # -------- .mrc OUTPUT ----------#
    my $path_output = substr($ARGV[1],0,rindex($ARGV[1],"/") ) if( index($ARGV[1],"/") >= 0 );
    my $filename_output = substr($ARGV[1],rindex($ARGV[1],"/"),length($ARGV[1]) ) if( defined $path_output );
    my $dir_output_marc = dir($path_output);
	my $file_output_marc = $dir_output_marc->file($filename_output);
	my $file_handle_output_marc = $file_output_marc->openw();
    
    # -------- READ .mrc -------------#
	my $batch = MARC::Batch->new('USMARC',$ARGV[0]);

	return ($file_handle_output_log,$file_handle_output_marc,$batch);
}




sub newFieldUpper10{
	my ($record,$field,$bibnumber,$file_handle_output_log) = @_;
	my $field_tag   = $field->tag();
	my @subfields = $field->subfields();
    my @newSubfields = ();
	while ( my $subfield = pop( @subfields ) ) 
	{
		my ($code,$data) = @$subfield;
        my $new_data = deleteUnicodeUnuse($data,$code,$bibnumber,$field_tag,$file_handle_output_log);
		push( @newSubfields, ($code, $new_data) );    
	}
	my $indicator_1 = $field->indicator(1) || '';
	my $indicator_2 = $field->indicator(2) || '';

    my $newField = MARC::Field->new($field_tag,$indicator_1,$indicator_2,@newSubfields);
    return $newField;
}




sub newFieldLower10
{
	my ($record,$field,$bibnumber,$file_handle_output_log) = @_;
	my $field_tag   = $field->tag();
	my @newSubfields = ();
    
	my $field_u10 = $record->field($field_tag);
	my $data = $field_u10->as_string() if( defined $field_u10 );
	if( defined $data )
	{
		my $new_data = $data;
	    $new_data = deleteUnicodeUnuse($data,'',$bibnumber,$field_tag,$file_handle_output_log);
	    return MARC::Field->new($field_tag,$new_data);
	}
}




sub deleteUnicodeUnuse
{
	my ($data,$code,$bibnumber,$field_tag,$file_handle_output_log) = @_;

	my $new_data = $data;
	$new_data =~ s/[\x00-\x1f]/ /g;
	if( $new_data ne $data )
    {
       my $print = $bibnumber	." EDIT ". $field_tag . $code ." Remove Unicode Control Char\n";
       $file_handle_output_log->print($print);      
    }
}




sub skipRecord
{
	my ($batch,$bibnumber) = @_;
	my @skip_record = ( '39423','39671' );
	
	foreach my $record (@skip_record) 
	{
		$batch->next() if( $bibnumber eq $record );
	}
}


































 
    