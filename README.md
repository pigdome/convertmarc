# README #


### How do I remigrate and remove ucc? ###

* copy folder xref 
* สร้าง folder ชื่อ src_mar , src_marc_removed , des_marc , log , logR ( เปลี่ยนชื่อได้ แต่ต้องแก้ใน GenSH.pl )
* $ perl GenSH.pl
* หากต้องการทำทีละ step ( 1 = remove unicode control charector , 2 = remigrate )
  $ perl GenSH_step_x.pl
* $ sh script.sh


### How do I import MARC to KOHA ? ###

* $ sudo koha-shell [ instance name ]
* $ ./tu_import_marc.pl -all -match_001 -l [ log file name for this file ] - file [ marc file name ]


