use strict;
#use warnings;
use Switch;
use Path::Class;
use autodie;
use Text::CSV;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;



open( OUTPUT_MARC, '> ' . $ARGV[1] ) or die $!;
my $logfilename = substr( $ARGV[1] , rindex( $ARGV[1] , "/" )+1 , length($ARGV[1]) );
open( OUTPUT_LOG , '> /home/sysadmin/kohaclone/convert/logR/log' . $logfilename . ".txt" ) or die $!;

my $batch = MARC::Batch->new('USMARC',$ARGV[0]);
$batch->strict_off();
$batch->warnings_off();
my $bibnumber;
my $DEBUG = 0;
my %HashOCLC = {};
my %HashItemCC = {};

prepareItemCC();
prepareOCLC();
convertErrMARCToMARC();

close( OUTPUT_MARC );
close( OUTPUT_LOG );




sub convertErrMARCToMARC
{
    my $record;
    eval { $record = $batch->next(); };
    while ( $record )
    {

        $bibnumber =  $record->field('001') || undef;#print "Biblionumber " . $bibnumber . " STEP 2 OK\n";
        if( defined $bibnumber )
        {
            $bibnumber = $bibnumber->as_string();
        }
        else
        {
            plog( "err after $bibnumber,It's have not tag 001" );  
        }
        

        # ------------Remove record if have not 949-------------#
        my $item = $record->field('949');
        if( ! defined $item )
        {
            plog("SKIP $bibnumber It's have not TAG 949\n"); 
            goto READ_BATCH;
        }

        #-------------Remove record if 008 is ok ---------------# 
        if( checkTag008($record) )
        {
            plog("SKIP $bibnumber TAG 008 is not ok \n");   
            goto READ_BATCH;
        }
        
        # ------------Remove tag 775 ---------------------------#
        $record->delete_field( $record->field('775') );
        
        # ----------- Tranfer 999 to 997 -----------------------#
        tranferTag999( $record );
        
    	# ------------Tranfer tag 001 --------------------------#
	    tranferTag001( $record ); 

	    # ------------Tranfer item type to 942 c ---------------#
        tranferItemType( $record );
        
        #------------ Tranfer 856 u,z to e,f -------------------#
        tranferTag856( $record );
        
        #------------ Change Tag 035 to OCLC -------------------#
        changeTag035ToOCLC( $record );
        
        my $rec_usmarc = $record->as_usmarc() || undef;
	    print OUTPUT_MARC $rec_usmarc if defined $rec_usmarc;
        
        READ_BATCH:
        if ( my @warnings = $batch->warnings() ) 
        {
            plog( "WARNINGS bib# $bibnumber:\n" . @warnings . "\n" );
        }
        do 
        {
           eval { $record = $batch->next(); };
           if ($@){ plog( "ERROR next to bib# $bibnumber (skipped):\n" . $@ . "\n" ); }
        } 
        while $@;
    }
        
}




sub checkTag008
{
    my ($record) = @_;
    my $t008 = $record->field('008');
    my $t050 = $record->field('050');
    my $t060 = $record->field('060');
    my $t082 = $record->field('082');
    my $t090 = $record->field('090');
    
    if( ! defined $t008 )
    {
    	return 1;
    }
    else
    {
    	$t008 = $t008->as_string();
    }
    if( index($t008,6) eq 's' && ! defined $t050 && ! defined $t060 && ! defined $t082 && ! defined $t090 )
    {
    	return 1;
    }
    return 0;
}





sub tranferTag001
{
	my ($record) = @_;
	my $field = $record->field('001');
	my $data = $field->as_string();
	if( defined $data )
	{
		my @newSubfields_999 = ();
	    push( @newSubfields_999, ('c', $data) );
	    push( @newSubfields_999, ('d', $data) );
	    my $new_field = MARC::Field->new('999','','',@newSubfields_999);
        if( defined $new_field )
        {
            $record->append_fields( $new_field );
        }
        else
        {
            plog("$bibnumber ERROR at new MARC Field tag 999\n");
        }

        my @newSubfields_988 = ();
        if( defined $record->field('988') )
        {
            @newSubfields_988 = $record->field('988')->subfields();
        }
	    push( @newSubfields_988, ('a', $data) );
        $new_field = MARC::Field->new('988','','',@newSubfields_988);
        if( defined $new_field )
        {
            $record->append_fields( $new_field );
        }
        else
        {
            plog("$bibnumber ERROR at new MARC Field tag 999\n");
        }
	    
	}
	   
}




sub tranferTag999
{
	my ($record) = @_;
	my $field_999 = $record->field('999');
    if( defined $field_999 )
    {
        my $indicator_1 = $field_999->indicator(1) || '';
        my $indicator_2 = $field_999->indicator(2) || '';

        my @subfields = $field_999->subfields();
        my @newSubfields = ();
        while ( my $subfield = pop( @subfields ) ) 
        {
            my ( $code , $data ) = @$subfield;
            push( @newSubfields, ( $code , $data ) ); 
        }
        my $new_field = MARC::Field->new('997',$indicator_1,$indicator_2,@newSubfields);
        if( defined $new_field )
        {
            $field_999->replace_with($new_field);
            $record->delete_field( $record->field('999') );
        }
        else
        {
            plog("$bibnumber ERROR at new MARC Field 997\n");
        }
        
    } 
}





sub tranferItemType
{
	my ($record) = @_;
	my $field_949 = $record->field('949');
    if( defined $field_949 )
    {
        my $data_t = $field_949->subfield('t');
        my $data_c = $field_949->subfield('c');
        my $indicator_1 = $field_949->indicator(1) || '';
        my $indicator_2 = $field_949->indicator(2) || '';
        
        foreach my $field ($record->field("9.."))
        {
           if ( $field->tag() == 949 ) 
           {
              my $newData_t = $field->subfield('t');
              if ( $newData_t gt $data_t ) 
              {
                  $data_t = $newData_t;
                  $data_c = $field->subfield('c');
                  $indicator_1 = $field->indicator(1) || '';
                  $indicator_2 = $field->indicator(2) || '';
              }
           }   
        }
        # --------------- Remove all 949 tag ----------------#
        foreach my $field ($record->field("9..")) 
        {
           if ( $field->tag() == 949 ){ $record->delete_field($field);} 
        }
        my $data = $data_c .":".$data_t;
        $data = $HashItemCC{"$data"};
        if( ! defined $data )
        { 
            $data = $data_t; 
        }
        else
        { 
            plog(" $bibnumber Can't find bibnumber in HashItemCC STATUS:NOT CHANGE \n");
        }
        my $new_field = MARC::Field->new('942',$indicator_1,$indicator_2,c=>$data);
        if( defined $new_field )
        {
            $record->append_fields( $new_field );
        }
        else
        {
            plog("$bibnumber ERROR at new MARC Field tag 942\n");
        }  
    }
}





sub tranferTag856
{
	my ($record) = @_;
	my $field_856 = $record->field('856');
    if( defined $field_856 )
    {
        my $data_u = $field_856->subfield('u');
        my $data_z = $field_856->subfield('z');
        my $indicator_1 = $field_856->indicator(1) || '';
        my $indicator_2 = $field_856->indicator(2) || '';
        
        my @subfields = $record->field('988')->subfields();
        my @newSubfields = ();
        while ( my $subfield = pop( @subfields ) ) 
        {
            my ( $code , $data ) = @$subfield;
            push( @newSubfields, ( $code , $data ) ); 
        }
        push( @newSubfields, ( 'e' , $data_u ) );
        push( @newSubfields, ( 'f' , $data_z ) );
        $record->delete_field( $record->field('856') );
        $record->delete_field( $record->field('988') );
        my $new_field = MARC::Field->new('988',$indicator_1,$indicator_2,@newSubfields);
        if( defined $new_field )
        {
            $record->append_fields( $new_field );
        }
        else
        {
            plog("$bibnumber ERROR at new MARC Field 988\n");
        }
    }
}






sub changeTag035ToOCLC
{
	my ($record) = @_;
	my $field_035 = $record->field('035');
    if( defined $field_035 )
    {
        my $indicator_1 = $field_035->indicator(1) || '';
        my $indicator_2 = $field_035->indicator(2) || '';
        my $bibnumber = $record->field('001')->as_string();
        my $data = $HashOCLC{"$bibnumber"};
       
        if( defined $data )
        {
        	$data = "(OCoLC)".$data;
        	my $new_field = MARC::Field->new('035',$indicator_1,$indicator_2,a=>$data);
            if( defined $new_field )
            {
                $field_035->replace_with($new_field);
            }
            else
            {
                plog("$bibnumber ERROR at new MARC Field tag 035\n");
            }
            
        }
        else
        {
            plog(" $bibnumber Can't find bibnumber in HashOCLC  STATUS:NOT CHANGE \n");
        }
        
    } 
}



sub prepareOCLC
{
	my $path = "/home/sysadmin/kohaclone/convert/xref/";
	opendir (DIR,$path) or die $!;
	while ( my $file = readdir(DIR) ) 
	{
		if( index( $file , ".txt" ) >= 0 )
		{
			$file = $path . $file;
 			open FILE, "<$file" or die $!;
	        my @lines = <FILE>;
	        
	        foreach my $line (@lines) 
	        {
	        	if( index( $line , "OCLC" ) >= 0 || index( $line , "Control" ) >= 0 ){ next; }
                
	        	my ( $key , $value ) = getKeyAndValueOCLC($line);#print $key . ":" . $value . "\n";
	            $HashOCLC{"$key"} = $value;
	        }
		}
    }
}



sub prepareItemCC
{

	open FILE, "</home/sysadmin/kohaclone/convert/mapItem.txt";
    my @lines = <FILE>;
    
    foreach my $line (@lines) 
    {
    	my ( $key , $value ) = getKeyAndValueItemCC($line);
        $HashItemCC{"$key"} = $value;
    }
		
}




sub getKeyAndValueOCLC
{
    my ( $line ) = @_;
	my @words = split / /, $line;
	my @newArray = ();
	foreach my $str (@words) 
	{
		if ( $str ne ' ' && $str ne '' ) 
		{
		    push @newArray , $str;
		}
		if( scalar @newArray == 2 )
		{
			return ($newArray[1],$newArray[0]);
		}
	}	
}

sub getKeyAndValueItemCC
{
    my ( $line ) = @_;
	my @words = split /\t/, $line;
	return ( $words[0] .":".$words[1] , $words[2] );
}




sub plog 
{
   my ($log) = @_;
   foreach my $x (@ARGV) 
   {
      if ($x eq '-d' || $x eq '-D') 
      {
         $DEBUG = 1;
      }
   }
   print OUTPUT_LOG $log if $DEBUG;     
}


































 
    