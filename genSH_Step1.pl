use strict;
    
my $path = "/home/sysadmin/kohaclone/convert/src_marc/";
opendir (DIR,$path) or die $!;
open( OUTPUT , '> script_step1.sh' ) or die $!;
while ( my $file = readdir(DIR) ) 
{
	if( index( $file , ".mrc" ) >= 0 )
	{
		print OUTPUT "perl -e 'print \" File $file Removing Unicode Control Charecter ...\\n \"' \n";
		print OUTPUT "sudo perl convert.pl  /home/sysadmin/kohaclone/convert/src_marc/$file /home/sysadmin/kohaclone/convert/src_marc_removed/$file -d\n";
        print OUTPUT "perl -e 'print \" File $file Complete \\n \"' \n";
	}
	
}
close DIR;
close OUTPUT;