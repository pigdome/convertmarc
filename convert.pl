use strict;
#use warnings;
use Switch;
use Path::Class;
use autodie;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;
use Try::Tiny;


open( OUTPUT_MARC, '> ' . $ARGV[1] ) or die $!;
my $logfilename = substr( $ARGV[1] , rindex( $ARGV[1] , "/" )+1 , length($ARGV[1]) );
open( OUTPUT_LOG , '> /home/sysadmin/kohaclone/convert/log/log' . $logfilename . ".txt" ) or die $!;

my $batch = MARC::Batch->new('USMARC',$ARGV[0]);
$batch->strict_off();
$batch->warnings_off();
my $bibnumber;
my $bibnumber_status = 1;
my $DEBUG = 0;
my @skipList = (
	            "615121","615152","554850","559965","565467","569235","575557",
	            "575726","576123","576308","576699","580636","588770","595864","85513",
	            "189280","295986"
	            );

convertErrMARCToMARC();

close( OUTPUT_MARC );
close( OUTPUT_LOG );

sub convertErrMARCToMARC
{
	my $record;
	eval { $record = $batch->next(); };
    while ( $record )
    {
    	$bibnumber =  $record->field('001')->as_string();#print "Biblionumber " . $bibnumber . " STEP 1 OK\n";
    	if( bibIsErr($bibnumber) )
    	{
    		goto READ_BATCH;
    	}
    	my $record_out = MARC::Record->new();
    	my $leader = $record->leader();
    	$record_out->leader($leader);
        
    	foreach my $field ($record->field("...")) 
	    {
	       my ( $newField , $log );
	       if( isNumeric( $field->tag() ) )
	       {
	       	   if( $field->tag() < 10 )
		       {
		       	   ($newField,$log) = newFieldLower10( $field , $record->field( $field->tag() ) ) ; 
		       }
		       else
		       {
	    		   ($newField,$log) = newFieldUpper10( $field );
		       }
	       }
	       else
	       {
	       	   plog("$bibnumber have err field " . $field->tag() );
	       }
	       
	       plog( $bibnumber . $log )if defined $log;
	       if( defined $newField )
	       {
	       	  $record_out->append_fields( $newField );
	       }
	       else
	       {
	       	  plog(" $bibnumber can't create new field ");
	       }
	       	
	    }	
	    
        try 
        {
        	print OUTPUT_MARC $record_out->as_usmarc();
		} 
		catch 
		{
        	plog( "caught error at bib $bibnumber: $_" );
		};

	    plog( $bibnumber ." OK \n" )if $bibnumber_status;
	    $bibnumber_status = 1;
	    
	    READ_BATCH:
	    if ( my @warnings = $batch->warnings() ) 
	    {
            plog( "WARNINGS bib# $bibnumber:" . @warnings . "\n" );
        }
	    do 
	    {
           eval { $record = $batch->next(); };
           if ($@){ plog( "ERROR next to bib# $bibnumber (skipped):\n" . $@ . "\n" ); }

           for (my $i = 0; $i < scalar @ARGV; $i++) {
           	   if ( $ARGV[$i] eq '-n' )
           	   {
 					exit if $bibnumber eq $ARGV[$i+1];
           	   }
           }
        } 
        while $@;

    }
        
}


sub newFieldUpper10
{
	my ( $field ) = @_;
	my $field_tag   = $field->tag();
	my @subfields = $field->subfields();
    my @newSubfields = ();
    my $log;
	while ( my $subfield = pop( @subfields ) ) 
	{
		my ( $code , $data ) = @$subfield;
		unless ( isSubfield( $code ) ) 
		{
			plog(" subfield incorrect : $code ");
			next;
		}
        my ( $new_data , $hasRemove ) = deleteUnicodeUnuse( $data , $code );
		push( @newSubfields, ( $code , $new_data ) );
		unless ($hasRemove) {
	    	$log .= " EDIT ". $field_tag . $code ." Remove Unicode Control Char\n";
	    }   
	}

	my $indicator_1 = $field->indicator(1) || '';
	my $indicator_2 = $field->indicator(2) || '';

    my $newField = MARC::Field->new($field_tag,$indicator_1,$indicator_2,@newSubfields) || undef;
    return ( $newField , $log );
}




sub newFieldLower10
{
	my ( $field , $field_u10 ) = @_;
	my $field_tag   = $field->tag();
	my @newSubfields = ();
    my $log;
	my $data = $field_u10->as_string();
	
    my ( $new_data , $hasRemove ) = deleteUnicodeUnuse( $data , '' );
    unless ($hasRemove) {
    	$log = " EDIT ". $field_tag ." Remove Unicode Control Char\n";
    }
    my $newField = MARC::Field->new($field_tag,$new_data) || undef;
    return ( $newField , $log );	
}




sub deleteUnicodeUnuse
{
	my ( $data , $code ) = @_;
    my $hasRemove = 1;
    my $new_data = $data;
	$new_data =~ s/[\x00-\x1f]/ /g;
	if( $new_data ne $data )
    {
       $bibnumber_status = 0;
       $hasRemove = 0;
    }
    return ( $new_data , $hasRemove );
}


sub isSubfield
{
	my ($str) = @_;
	foreach my $x ( split //, $str ) 
	{
		$x = lc $x;
		unless ( $x =~ /[0-9]/ || $x =~ /[a-z]/ ) 
		{
			return 0;
		}
	}
	return 1;
}


sub isNumeric
{
	my ($str) = @_;
	foreach my $x ( split //, $str ) 
	{
		unless ( $x =~ /[0-9]/ ) 
		{
			return 0;
		}
	}
	return 1;
}

sub bibIsErr
{
	my ($bibnumber) = @_;
  	foreach my $rec (@skipList)
  	{
  		if( $bibnumber eq $rec )
  		{
  			return 1;
  		}
  	}
  	return 0;
}

sub plog 
{
   my ($log) = @_;
   foreach my $x (@ARGV) 
   {
   	  if ($x eq '-d' || $x eq '-D') 
   	  {
   	  	 $DEBUG = 1;
   	  }
   }
   print OUTPUT_LOG $log if $DEBUG; 	
}









































 
    